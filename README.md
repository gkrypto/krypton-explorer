PHP block explorer for Krypton KR
=========================================

Inspired by:

https://github.com/etherparty/explorer
https://github.com/pharesim/shift_explorer


Copy config/config.php.default to config/config.php and edit according to your needs

WEB: index.php


Dependencies
------------
php-curl


ToDo
----
create cli templates